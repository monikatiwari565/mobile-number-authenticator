import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { formatCurrency } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  registerForm:FormGroup;
 constructor(private routes:Router) {
   this.registerForm= new FormGroup({
     Username:new FormControl(''),
     email:new FormControl(''),
     pwd:new FormControl(''),
     mobilenumber:new FormControl('')
   });
}

  ngOnInit(){
  }
  cancelClick()
  {
    this.routes.navigate([''])
  }
  submiClick(){
    console.log(this.registerForm.value)
  }
}
