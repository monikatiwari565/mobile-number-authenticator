import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup;
  constructor(private routes:Router) {
    this.loginForm=new FormGroup({
    email:new FormControl('',[Validators.required,Validators.email]),
    pwd:new FormControl('')
    });
   }

  ngOnInit() {
    
  }
  registerClick(){
    this.routes.navigate(["registration"])
  }
  onSubmit(){
    console.log(this.loginForm)
  }
  mobileClick(){
    this.routes.navigate(["loginMobile"])
  }
}
