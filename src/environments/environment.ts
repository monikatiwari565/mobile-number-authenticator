// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBk9KdD8WUWFzFnfqL2Ba_cMp0D--VDInQ",
    authDomain: "mobile-number-authentica-3055a.firebaseapp.com",
    databaseURL: "https://mobile-number-authentica-3055a.firebaseio.com",
    projectId: "mobile-number-authentica-3055a",
    storageBucket: "mobile-number-authentica-3055a.appspot.com",
    messagingSenderId: "153842971844",
    appId: "1:153842971844:web:12d7edb223e3b39a268591",
    measurementId: "G-TEYFHVPYKE"
  }
  // Initialize Firebase
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
