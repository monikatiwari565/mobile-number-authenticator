import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginMobileComponent } from './login-mobile/login-mobile.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  {path:'', component:LoginComponent},
  {path:'registration',component:RegistrationComponent},
  {path:'loginMobile',component:LoginMobileComponent},
  {path:'home',component:HomeComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
