import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-mobile',
  templateUrl: './login-mobile.component.html',
  styleUrls: ['./login-mobile.component.scss']
})
export class LoginMobileComponent implements OnInit {
  windowRef:any;
  phoneNumber:any;
  otp:any;
  constructor(private authRef:AngularFireAuth,private router:Router) {
    this.windowRef=window;
   }
   
  ngOnInit(): void {
    this.windowRef.recaptchaVerifire=new auth.RecaptchaVerifier('recaptcha container'),{
      size:'normal',
      callback:(res)=>{

      }

    }
    this.windowRef.recaptchaVerifire.render()
  }
  
  sendOTP(){
    this.authRef.signInWithPhoneNumber(this.phoneNumber,this.windowRef.recaptchaVerifire).then((confirmationResult)=>{
      this.windowRef.confirmationResult=confirmationResult;
      console.log("confirmationResult",confirmationResult)
    })
  }
  vrifyOTP(){
    this.windowRef.confirmationResult.confirm(this.otp).then((userCredential)=>{
      console.log(userCredential)
      if(userCredential.operationType==='signIn')
      {
        this.router.navigate(['home'])
      }
      else{
        alert("Please Enter Correct OTP")
      }
    },(error)=>{
      alert("Please Enter Correct OTP")

    })
  }
}
